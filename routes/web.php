<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\AuthCheck;
use App\Http\Middleware\NotLoggedIn;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AllUserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OptionController;
use App\Http\Controllers\OptionValueController;
use App\Http\Controllers\AttributeController;
use App\Http\Controllers\ProductAttributeController;
// use App\Http\Controllers\RolePermissionController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin/login',[UserController::class,'login'])->middleware('notLoggedIn');

Route::post('/check',[UserController::class,'check'])->name('check');
Route::get('/logout',[UserController::class,'logout']);

Route::group(['middleware'=>'authCheck'], function (){
    Route::get('/admin',[UserController::class,'admin']);

    Route::resource('/admin/profile',ProfileController::class);
    Route::resource('/admin/users',AllUserController::class);
    Route::resource('/admin/roles',RoleController::class);
    Route::resource('/admin/permissions',PermissionController::class);
    Route::get('/admin/products/get-option-values',[ProductController::class,'getOptionValues']);
    Route::resource('/admin/products',ProductController::class);
   
    Route::resource('/admin/options',OptionController::class);
    Route::resource('/admin/option-values',OptionValueController::class);
    Route::resource('/admin/attributes',AttributeController::class);
    Route::resource('/admin/attribute-values',ProductAttributeController::class);
    // Route::resource('/admin/role_permissions',RolePermissionController::class);
});
