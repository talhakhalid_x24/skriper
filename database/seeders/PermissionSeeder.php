<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [[
            'id'=>1,
            'permission_name'=>'add',
            'permission_module'=>'role',
            'permission_key'=>'role-add',
        ],[
            'id'=>2,
            'permission_name'=>'edit',
            'permission_module'=>'role',
            'permission_key'=>'role-update',
        ],[
            'id'=>3,
            'permission_name'=>'delete',
            'permission_module'=>'role',
            'permission_key'=>'role-delete',
        ],[
            'id'=>4,
            'permission_name'=>'add',
            'permission_module'=>'product',
            'permission_key'=>'product-add',
        ],[
            'id'=>5,
            'permission_name'=>'edit',
            'permission_module'=>'product',
            'permission_key'=>'product-update',
        ],[
            'id'=>6,
            'permission_name'=>'delete',
            'permission_module'=>'product',
            'permission_key'=>'product-delete',
        ],[
            'id'=>7,
            'permission_name'=>'add',
            'permission_module'=>'user',
            'permission_key'=>'user-add',
        ],[
            'id'=>8,
            'permission_name'=>'edit',
            'permission_module'=>'user',
            'permission_key'=>'user-update',
        ],[
            'id'=>9,
            'permission_name'=>'delete',
            'permission_module'=>'user',
            'permission_key'=>'user-delete',
        ],[
            'id'=>10,
            'permission_name'=>'add',
            'permission_module'=>'option',
            'permission_key'=>'option-add',
        ],[
            'id'=>11,
            'permission_name'=>'edit',
            'permission_module'=>'option',
            'permission_key'=>'option-update',
        ],[
            'id'=>12,
            'permission_name'=>'delete',
            'permission_module'=>'option',
            'permission_key'=>'option-delete',
        ],[
            'id'=>13,
            'permission_name'=>'add',
            'permission_module'=>'attribute',
            'permission_key'=>'attribute-add',
        ],[
            'id'=>14,
            'permission_name'=>'edit',
            'permission_module'=>'attribute',
            'permission_key'=>'attribute-update',
        ],[
            'id'=>15,
            'permission_name'=>'delete',
            'permission_module'=>'attribute',
            'permission_key'=>'attribute-delete',
        ]];
        foreach($permission as $permissions){
            DB::table('permissions')->insert($permissions);
        }
    }
}
