<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'=>1,
            'name'=>'Admin',
            'email'=>'admin@admin.com',
            'password'=>Hash::make('admin123'),
            'image'=>null,
            'role_id'=>1,
            'isAdmin'=>1,
        ]);
    }
}
