<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Skriper | Login</title>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Main styles for this application-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
  </head>
  <body class="c-app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-sm-6">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1>Sign In</h1>
                <p class="text-muted">Sign In to your account</p>
                <form action="{{ route('check') }}" method="post">
                  @csrf
                  <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">
                        <svg class="c-icon">
                          <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-user')}}"></use>
                        </svg></span></div>
                    <input class="form-control" type="email" placeholder="Enter Your Email" name="email">
                  </div>
                  @error('email')
                  <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                  @enderror
                  <div class="input-group mt-3">
                    <div class="input-group-prepend"><span class="input-group-text">
                        <svg class="c-icon">
                          <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-lock-locked')}}"></use>
                        </svg></span></div>
                    <input class="form-control" type="password" placeholder="Enter Your Password" name="password">
                  </div>
                  @error('password')
                      <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                  @enderror
                  <div class="row">
                    <div class="col-6">
                      <button class="btn btn-primary px-4 mt-3" type="submit">Sign In</button>
                    </div>
                  </div>
                  @if(Session::get('fails'))
                  <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                      <strong>{{Session::get('fails')}}</strong>
                      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                  </div>
                  @endif
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="{{asset('vendors/@coreui/coreui/js/coreui.bundle.min.js')}}"></script>
    <!--[if IE]><!-->
    <script src="{{asset('vendors/@coreui/icons/js/svgxuse.min.js')}}"></script>
    <!--<![endif]-->

    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

  </body>
</html>