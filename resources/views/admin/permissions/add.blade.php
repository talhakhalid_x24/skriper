@extends('layouts/admin_web')
<title>Skriper | Permissions | Add</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Add Permission</h1>
                </div>
                <form action="{{ route('permissions.store') }}" method="POST">
                    @csrf
                    <div class="col-sm-12 position-relative">
                        <label for="validationTooltip03" class="form-label">Permission Name</label>
                        <div class="input-group has-validation">
                        <input type="text" class="form-control" name="permission_name">
                        </div>
                        @error('permission_name')
                            <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-sm-12 position-relative">
                        <label for="validationTooltip03" class="form-label">Permission Module</label>
                        <div class="input-group has-validation">
                        <input type="text" class="form-control" name="permission_module">
                        </div>
                        @error('permission_module')
                            <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-sm-12 position-relative">
                        <label for="validationTooltip03" class="form-label">Permission Key</label>
                        <div class="input-group has-validation">
                        <input type="text" class="form-control" name="permission_key">
                        </div>
                        @error('permission_key')
                            <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-12 my-3">
                        <button class="btn btn-success" type="submit"><i class="fas fa-plus"></i> Add</button>
                    </div>
                </form> 
@endsection