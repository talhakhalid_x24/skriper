@extends('layouts/admin_web')
<title>Skriper | Permissions</title>
@section('content')
                <div class="col-12 mb-3">
                <h1>Permissions</h1>
                    <!-- <a href="{{ route('permissions.create') }}"><button class="btn btn-info my-3 text-light"><i class="fas fa-plus"></i> Add</button></a> -->
                </div>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{Session::get('success')}}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                <div class="card">
                <div class="headeing-content"><h5><i class="fal fa-list-ul me-1"></i> Permission List</h5></div>
                <div class="col-12">   
                <table id="userCategories" class="table table-responsive-sm table-hover table-bordered text-center">
                    <thead class="table-dark">
                        <tr>
                            <td>Id</td>
                            <td>Permission Name</td>
                            <td>Permissions Module</td>
                            <td>Permissions Key</td>
                            <!-- <td>Action</td> -->
                        </tr>
                    </thead>
                    <tbody class="table-secondary text-secondary">
                    @foreach($permission as $permissions)
                        <tr>
                            <td>{{$permissions->id}}</td>
                            <td>{{$permissions->permission_name}}</td>
                            <td>{{$permissions->permission_module}}</td>
                            <td>{{$permissions->permission_key}}</td>
                            <!-- <td class="d-flex"><a href="{{ route('permissions.edit', $permissions->id) }}" class="me-2"><button class="btn btn-success"><i class="fas fa-edit"></i> Edit </button></a>
                            <form action="{{ route('permissions.destroy', $permissions->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"><i class="fas fa-trash"></i> Delete</button>
                            </form>
                            </td> -->
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                </div>
@endsection