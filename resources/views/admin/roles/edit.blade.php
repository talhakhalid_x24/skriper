@extends('layouts/admin_web')
<title>Skriper | Roles | Update</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Update Role</h1>
                </div>
                <div class="card">
                <div class="headeing-content"><h5 class="m-auto"><i class="fal fa-list-ul me-1"></i> Edit role</h5></div>
                <form action="{{ route('roles.update' , $roles->id) }}" method="POST">
                @csrf
                @method('PATCH')
                    <div class="col-sm-12 position-relative">
                        <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Role</label>
                        <div class="input-group has-validation">
                        <input type="text" class="form-control" name="role" value="{{$roles->role}}">
                        </div>
                        @error('role')
                            <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-sm-6 position-relative">
                        <label for="validationTooltip03" class="form-label">Permissions</label>
                        <table class="table table-responsive-sm table-hover table-bordered text-center">
                            <div class="input-group has-validation">
                            <thead class="table-dark">
                            <tr>
                                <th>#</th>
                                <th>Permission</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($permission as $permissions)
                                <tr>
                                    <div class="me-2">
                                        <td>
                                            <input type="checkbox" value="{{$permissions->id}}" name="permissions[]" 
                                            <?php foreach($role_permission as $role_permissions){
                                                if($permissions->id == $role_permissions->permission_id){
                                                    echo "checked";
                                                } } ?> 
                                            >
                                        </td>
                                        <td>
                                            <label for="">{{$permissions->permission_key}}</label>
                                        </td>
                                    </div>
                                </tr>
                                @endforeach
                            </tbody>
                            </div>
                        </table>
                        @error('permissions')
                            <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-sm-12 my-3">
                        <button class="btn btn-success" type="submit"><i class="fas fa-edit"> </i> Update</button>
                    </div>
                </form>
                </div>
@endsection