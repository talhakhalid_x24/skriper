@extends('layouts/admin_web')
<title>Skriper | Options</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Option Values</h1>
                </div>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{Session::get('success')}}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                <div class="card">
                <div class="headeing-content"><h5><i class="fal fa-list-ul me-1"></i> Option Value List</h5></div>
                <div class="col-12">    
                    <table id="userCategories" class="table table-responsive-sm table-hover table-bordered text-center">
                        <thead class="table-dark">
                            <tr>
                                <th>Id</th>
                                <th>Option Names</th>
                                <th>Option Values</th>
                            </tr>
                        </thead>
                        <tbody class="table-secondary text-secondary">
                            @foreach($option_value as $option_values)
                                <tr>
                                    <td>{{$option_values['id']}}</td>
                                    <td>{{$option_values['option']['option_name']}}</td>
                                    <td>{{$option_values['option_value']}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>

@endsection