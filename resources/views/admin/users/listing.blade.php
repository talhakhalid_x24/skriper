@extends('layouts/admin_web')
<title>Skriper | Users</title>

@section('content')
                <div class="col-12 mb-3 d-flex justify-content-between">
                    <h1>Users</h1>
                        <?php 
                            foreach (session()->get('permissionKey') as $key) {
                                if($key['permission_key'] == 'user-add'){ 
                        ?>
                    <a href="{{ route('users.create') }}"><button class="btn btn-info text-light"><i class="fas fa-plus"></i> Add</button></a>
                        <?php } }  ?>
                </div>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{Session::get('success')}}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                <div class="card">
                    <div class="headeing-content"><h5><i class="fal fa-list-ul me-1"></i> User List</h5></div>
                    <div class="col-12">    
                        <table id="userCategories" class="table table-responsive-sm table-hover table-bordered text-center">
                            <thead class="table-dark">
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Image</th>
                                    <th>Role</th>
                                    <th>Super Admin</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="table-secondary text-secondary">
                                @foreach($user as $users)
                                    <tr>
                                        <td>{{$users['id']}}</td>
                                        <td>{{$users['name']}}</td>
                                        <td>{{$users['email']}}</td>
                                        <td>
                                            @if($users['image'])
                                            <img src="{{asset('storage/user/'.$users['image'])}}" height="35" alt="No Image">
                                            @else
                                            <i class="fad fa-user" style="font-size:35px;"></i>
                                            @endif
                                        </td>
                                            <td>{{($users['all_role']['role'])}}</td>
                                            <td>
                                            <?php if($users['isAdmin'] == 1){
                                                echo 'Yes';
                                            }
                                            else{
                                                echo 'No';
                                            }
                                            ?>
                                        </td>
                                        <td class="d-flex justify-content-center">
                                            <?php 
                                                foreach (session()->get('permissionKey') as $key) {
                                                    if($key['permission_key'] == 'user-update'){ 
                                            ?>
                                            <a href="{{ route('users.edit', $users->id) }}" class="my-auto me-2"><button class="btn btn-success"><i class="fas fa-edit"></i> Edit </button></a>
                                            <?php 
                                                    }
                                                    if($key['permission_key'] == 'user-delete'){ 
                                            ?>
                                            <form action="{{ route('users.destroy', $users->id) }}" method="post" class="my-auto">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger"><i class="fas fa-trash"></i> Delete</button>
                                            </form>  
                                            <?php } } ?>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

@endsection