@extends('layouts/admin_web')
<title>Skriper | Users | Add</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Add User</h1>
                </div>
                <div class="card">
                <div class="headeing-content"><h5 class="m-auto"><i class="fal fa-list-ul me-1"></i> Add user</h5></div>
                <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Name:</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="name">
                    </div>
                    @error('name')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Email:</label>
                    <div class="input-group has-validation">
                    <input type="email" class="form-control" name="email">
                    </div>
                    @error('email')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Password:</label>
                    <div class="input-group has-validation">
                    <input type="password" class="form-control" name="password">
                    </div>
                    @error('password')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"> Image:</label>
                    <div class="input-group has-validation">
                    <input type="file" class="form-control" name="image">
                    </div>
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Role:</label>
                    <div class="input-group has-validation">
                    <select name="role_id" class="form-control">
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->role}}</option>
                        @endforeach
                    </select>
                    </div>
                    @error('role_id')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                    <div class="col-sm-12 my-3">
                        <button class="btn btn-success" type="submit"><i class="fas fa-plus"></i> Add</button>
                    </div>
                </form> 
                </div>
@endsection