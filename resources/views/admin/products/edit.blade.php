@extends('layouts/admin_web')
<title>Skriper | Product | Update</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Update Product</h1>
                </div>
                <div class="card">
                <div class="headeing-content"><h5 class="m-auto"><i class="fal fa-list-ul me-1"></i> Edit product</h5></div>
                <form action="{{ route('products.update' , $products['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Name</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="product_name" value="{{$products['product_name']}}">
                    </div>
                    @error('product_name')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Desciption</label>
                    <div class="input-group has-validation">
                    <textarea class="form-control" name="product_description">{{$products['product_description']}}</textarea>
                    </div>
                    @error('product_description')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Price</label>
                    <div class="input-group has-validation">
                    <input type="number" class="form-control" name="price" value="{{$products['price']}}">
                    </div>
                    @error('price')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Link</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="link" value="{{$products['link']}}">
                    </div>
                    @error('product_link')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Images</label>
                    <a class="btn btn-success text-light float-right" onclick="createImage()">+ Add Another Image</a>
                    <div class="input-group has-validation">
                    @foreach($product_images as $images)
                    <div class="alert alert-dismissible fade show p-0" style="width:100%;">
                        <input type="file" class="form-control" name="product_image[]" value="{{asset('storage/product_images/'.$images['product_images'])}}">
                        <button type="button" class="close pt-2" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endforeach
                    </div>
                    <div id="newElementId1"></div>
                    @error('product_image')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Featured</label>
                    <div class="input-group has-validation">
                    <select name="is_featured"  class="form-control">
                        <option value="0">Not Active</option>
                        <option value="1">Active</option>
                    </select>
                    </div>
                    @error('is_featured')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <br>
                <a class="btn btn-success checked text-light float-right me-3"><i class="fas fa-plus"></i> Add Another Attribute Value</a>
                @foreach($productAttribute as  $productAttributes)
                <div class="row position-relative col-sm-12 clone alert alert-dismissible fade show pb-0 mb-0">
                    <div class="col-sm-5">
                        <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Attributes Name</label>
                        <div class="input-group has-validation">
                            <select name="attribute[]" class="form-control">
                                @foreach($attribute as $attributes)
                                    <option value="{{$attributes->id}}"
                                        <?php 
                                            if($attributes->id == $productAttributes->attribute_id){
                                                echo 'selected';
                                            }
                                        ?>
                                    >{{$attributes->attribute_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Attributes Value</label>
                        <div class="input-group has-validation">
                            <input type="text" class="form-control" value="{{$productAttributes->attribute_value}}" name="attribute_value[]">
                        </div>
                    </div>
                    <div class="col-sm-2">
                    <button type="button" class="rounded btn btn-danger" style="height: 40px;margin-top: 30px;" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-trash"></i> Delete</button>
                    </div>
                    @error('attribute')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                @endforeach
                <div class="add-attribute-value"></div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Option Name</label>
                    <div class="input-group has-validation">
                        @foreach($option as $options)
                            <div class="me-2">
                                <input type="checkbox" value="{{$options->id}}" name="option[]"
                                <?php
                                foreach($productOption as $pro){
                                    if($options->id == $pro->option_id){
                                        echo 'checked';
                                    }
                                }
                                ?> >
                                <label for="">{{$options->option_name}}</label>
                            </div>
                        @endforeach
                    </div>
                    @error('option')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                    <div class="col-12 my-3">
                        <button class="btn btn-success" type="submit"><i class="fas fa-edit"> </i> Update</button>
                    </div>
                </form>
@endsection