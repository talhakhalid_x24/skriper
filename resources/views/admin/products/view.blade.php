@extends('layouts/admin_web')
@section('content')
@foreach($product as $products)
<title>Skriper | Products | {{$products['product_name']}}</title>


    <div class="col-12 mb-3 d-flex justify-content-between">
        <h1>Product <span class="text-secondary"> | {{$products['product_name']}}</span></h1>

    </div>
    <div class="mt-5 row">
        <div class="col-sm-4">
            <img id="expandedImg" style="width:100%;border: 2px solid darkgray;" src="{{ asset('storage/product_images/'.$products['productImages'][0]['product_images']) }}">
        </div>
        <div class="col-sm-8 my-auto">
            <h1 class="mb-4" style="font-size:60px;">{{$products['product_name']}}</h1>
            <p class="mb-4 fw-bold">{{$products['product_description']}}</p>
            <p class="fw-bold fs-5 m-0">Price : {{$products['price']}}</p>
            <span>
                @if($products['productImages'][0]['is_featured'] == 0) 
                    <span class="text-danger"><i class="fas fa-circle"></i>  Not Active</span> 
                @else 
                    <span class="text-success"><i class="fas fa-circle"></i> Active</span> 
                @endif
            </span>
            <p class="text-primary fw-bold m-0"><a href="{{$products['link']}}" target="_blank" class="text-decoration-none">Check out this {{$products['product_name']}}</a></p>
        </div>
        @foreach($products['productImages'] as $images)
            <div class="column">
                <img src="{{ asset('storage/product_images/'.$images['product_images']) }}" alt="Nature" style="width:100%;height:100px;" onclick="myFunction(this);">
            </div>
        @endforeach
    </div>
@endforeach

@endsection