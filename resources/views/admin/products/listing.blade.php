@extends('layouts/admin_web')
<title>Skriper | Prdoucts</title>

@section('content')
    <div class="col-12 mb-3 d-flex justify-content-between">
        <h1>Products</h1>
        <?php 
            foreach (session()->get('permissionKey') as $key) {
                if($key['permission_key'] == 'product-add'){ 
        ?>

            <a href="{{ route('products.create') }}"><button class="btn btn-info my-3 text-light"><i class="fas fa-plus"></i> Add</button></a>
        <?php } }  ?>
    </div> 
    @if(Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{Session::get('success')}}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @foreach($product as $products)
        <div class="col-4 mx-auto card" style="width:31%;">
            <img style="height:200px;" src="{{ asset('storage/product_images/'.$products['productImages'][0]['product_images']) }}" alt="">
            <div class="card-body">
                <h5 class="card-title">{{$products['product_name']}}</h5>
                <p class="card-text">{{substr($products['product_description'],0,100)}} ...</p>
                <div class="d-flex justify-content-between">
                    <a href="{{ route('products.show', $products['id']) }}"><button class="btn btn-primary"><i class="fas fa-eye"></i> View </button></a>
                    <?php 
                        foreach (session()->get('permissionKey') as $key) {
                            if($key['permission_key'] == 'product-update'){ 
                    ?>
                        <a href="{{ route('products.edit', $products['id']) }}"><button class="btn btn-success"><i class="fas fa-edit"></i> Edit </button></a>
                    <?php 
                        }
                        if($key['permission_key'] == 'product-delete'){ 
                    ?>
                        <form action="{{ route('products.destroy', $products['id']) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> Delete</button>
                        </form>  
                    <?php } } ?>
                </div>
            </div>
        </div>
    @endforeach
        <div>
            {{$product->links('pagination/bootstrap-4')}}
        </div>
@endsection 