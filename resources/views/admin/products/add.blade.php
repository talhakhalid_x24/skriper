@extends('layouts/admin_web')
<title>Skriper | Product | Add</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Add Product</h1>
                </div>
                <div class="card">
                <div class="headeing-content"><h5 class="m-auto"><i class="fal fa-list-ul me-1"></i> Add product</h5></div>
                <form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Name</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="product_name">
                    </div>
                    @error('product_name')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Desciption</label>
                    <div class="input-group has-validation">
                    <textarea id="editor" class="form-control" name="product_description"></textarea>
                    </div>
                    @error('product_description')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Price</label>
                    <div class="input-group has-validation">
                    <input type="number" class="form-control" name="price">
                    </div>
                    @error('price')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Link</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="link">
                    </div>
                    @error('product_link')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Product Images</label>
                    <div class="input-group has-validation">
                    <input type="file" class="form-control" name="product_image[]">
                    <a class="btn btn-success text-light" onclick="createImage()">+ Add Another Image</a>
                    </div>
                    <div id="newElementId1"></div>
                    @error('product_image')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <br>
                <div class="headeing-content"><h5 class="m-auto"><i class="fal fa-list-ul me-1"></i> Add attribute</h5></div>
                <div class="row position-relative clone alert alert-dismissible fade show pb-0 mb-0">
                    <div class="col-sm-5">
                        <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Attributes Name</label>
                        <div class="input-group has-validation">
                            <select name="attribute[]" class="form-control">
                                @foreach($attribute as $attributes)
                                    <option value="{{$attributes->id}}">{{$attributes->attribute_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Attributes Value</label>
                        <div class="input-group has-validation">
                            <input type="text" class="form-control" name="attribute_value[]">
                        </div>
                    </div>
                    <div class="col-sm-1 mt-auto mx-auto">
                    <button type="button" class="rounded btn btn-danger" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-trash"></i></button>
                    </div>
                    <div class="col-sm-1 mt-auto mx-auto">
                        <a class="btn btn-success checked text-light"><i class="fas fa-plus"></i></a>
                    </div>
                    @error('attribute')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="add-attribute-value"></div>
                <br>
                <div class="headeing-content"><h5 class="m-auto"><i class="fal fa-list-ul me-1"></i> Add option</h5></div>
                <div class="row position-relative clone-option alert alert-dismissible fade show pb-0 mb-0">
                    <div class="col-sm-5 option">
                        <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Option Name</label>
                        <div class="input-group has-validation">
                            <select name="option[]" class="form-control" id="selectOption">
                                <option>Select Option</option>
                                    @foreach($option as $options)
                                        <option value="{{$options->id}}">{{$options->option_name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5 option-value">
                        <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Option Value</label>
                        <div class="input-group has-validation">
                        <select name="option_value[]" class="form-control" id="selectOptionValue">
                                
                        </select>
                        </div>
                    </div>
                    <div class="col-sm-1 mt-auto mx-auto">
                    <button type="button" class="rounded btn btn-danger" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-trash"></i></button>
                    </div>
                    <div class="col-sm-1 mt-auto mx-auto">
                        <a class="btn btn-success checked-option text-light"><i class="fas fa-plus"></i> </a>
                    </div>
                    @error('option')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="add-option-value"></div>
                    <div class="col-sm-12 my-3">
                        <button class="btn btn-success" type="submit"><i class="fas fa-plus"></i> Add</button>
                    </div>
                </form>
                </div>
@endsection
