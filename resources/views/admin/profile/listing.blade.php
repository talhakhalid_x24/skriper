@extends('layouts/admin_web')
<title>Skriper | Profile</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Proflie</h1>
                </div>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{Session::get('success')}}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                <div class="card pb-5">
                <div class="headeing-content"><h5><i class="fal fa-list-ul me-1"></i> Profile</h5></div>
                    <div class="profile d-flex justify-content-around">
                    <div class="my-auto">
                        @if(session()->get('LoggedIn')['image'])
                        <div>
                            <form action="{{ route('profile.update', session()->get('LoggedIn')['id']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <img class="rounded-circle" style="height:300px; width:100%" src="{{asset('storage/user/'.session()->get('LoggedIn')['image'])}}" alt="user@email.com">
                                <br><br><div style="position:relative;"><input type="file" class="photo" name="image" required><span class="upload btn btn-danger">Upload Your Photo</span></div><br>
                                <br><button class="w-100 btn btn-success"><i class="fas fa-edit"></i> Upload </button> 
                            </form>
                        </div>
                        
                        @else
                        <div>
                            <form action="{{ route('profile.update', session()->get('LoggedIn')['id']) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <i class="fad fa-user text-secondary mb-4" style="font-size: 250px;margin-top: 15px;"></i>
                                <br><div style="position:relative;"><input type="file" class="photo" name="image" required><span class="upload btn btn-danger">Upload Your Photo</span></div><br>
                                <br><button class="w-100 btn btn-success"><i class="fas fa-edit"></i> Upload </button> 
                            </form>
                        </div>
                        @endif
                    </div>
                    <div class="mt-5">
                        <form action="{{ route('profile.update', session()->get('LoggedIn')['id']) }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="form-group d-flex">
                                <label for="" class="text-secondary me-2 my-auto">Name:</label>
                                <input type="text" value="{{session()->get('LoggedIn')['name']}}" name="name" class="form-control">
                            </div>
                            <div class="form-group d-flex">
                                <label for="" class="text-secondary me-2 my-auto">Email:</label>
                                <input type="text" value="{{session()->get('LoggedIn')['email']}}" name="email" class="form-control">
                            </div>
                            <div class="col-12">
                                <button class="btn btn-success float-right"><i class="fas fa-edit"></i> Edit Your Details </button>
                            </div>
                        </form>
                        <br>
                        <form action="{{ route('profile.update', session()->get('LoggedIn')['id']) }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <label for="" class="text-secondary mb-2 me-2 my-auto">Old Password:</label>
                                <input type="password" name="old_pass" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-secondary mb-2 me-2 my-auto">New Password:</label>
                                <input type="password" name="new_pass" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-secondary mb-2 me-2 my-auto">Confirm Password:</label>
                                <input type="password" name="con_pass" class="form-control" required>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-success float-right"><i class="fas fa-edit"></i> Change Your Password </button>
                            </div>
                            @if(Session::get('err'))
                                <div class="alert alert-danger alert-dismissible fade show float-left w-100 mt-3" role="alert">
                                    <strong>{{Session::get('err')}}</strong>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                        </form>
                        <br>
                    </div>
                    <!-- <div class="col-sm-6 d-flex">
                        <label for="" class="fs-2 text-secondary">Full Name: </label>
                        <h1 class="ms-3">{{session()->get('LoggedIn')['name']}}</h1>
                    </div>
                    <div class="col-12 d-flex">
                        <label for="" class="fs-2 text-secondary">Email Address: </label>
                        <p class="fs-2 ms-3 text-secondary ">  {{session()->get('LoggedIn')['email']}}</p>
                    </div>
                    <div class="col-12 d-flex">
                        <label for="" class="fs-2 text-secondary">Password: </label>
                        <p class="fs-2 ms-3 text-secondary">xxxxxxxxxxxxxxxxxxxxxxx</p>
                    </div>
                    <div class="col-12 d-flex">
                        <label for="" class="fs-2 text-secondary">Super Admin: </label>
                        <p class="fs-2 ms-3 text-secondary "> 
                            @if(session()->get('LoggedIn')['isAdmin'] == 1)
                            You are Super Admin
                            @else
                                You are not super admin
                            @endif
                        </p>
                    </div>
                    <div class="col-12">
                        <a href="{{ route('profile.edit', session()->get('LoggedIn')['id']) }}" class="my-auto me-2"><button class="btn btn-success px-5"><i class="fas fa-edit"></i> Edit </button></a>
                    </div> -->
                </div>
                </div>

@endsection