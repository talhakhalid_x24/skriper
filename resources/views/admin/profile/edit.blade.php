@extends('layouts/admin_web')
<title>Skriper | Profile | Update</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Update Your Profile</h1>
                </div>
                <form action="{{ route('profile.update' , $user->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label">Name</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="name" value="{{$user->name}}">
                    </div>
                    @error('name')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label">Email</label>
                    <div class="input-group has-validation">
                    <input type="email" class="form-control" name="email" value="{{$user->email}}">
                    </div>
                    @error('email')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label">Password</label>
                    <div class="input-group has-validation">
                    <input type="password" class="form-control" name="password" value="{{$user->password}}">
                    </div>
                    @error('password')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label">Image <span style="color:lightgray;">(Optional)</span></label>
                    <div class="input-group has-validation">
                    <input type="file" class="form-control" name="image" value="{{$user->image}}">
                    </div>
                </div>
                <div class="col-12 my-3">
                    <button class="btn btn-success" type="submit"><i class="fas fa-edit"> </i> Update</button>
                </div>
                </form>
@endsection