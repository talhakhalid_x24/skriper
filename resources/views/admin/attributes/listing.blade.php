@extends('layouts/admin_web')
<title>Skriper | Attributes</title>

@section('content')
                <div class="col-12 mb-3 d-flex justify-content-between">
                    <h1>Attributes</h1>
                        <?php 
                            foreach (session()->get('permissionKey') as $key) {
                                if($key['permission_key'] == 'attribute-add'){ 
                        ?>
                    <a href="{{ route('attributes.create') }}"><button class="btn btn-info text-light"><i class="fas fa-plus"></i> Add</button></a>
                        <?php } }  ?>
                </div>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{Session::get('success')}}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                <div class="card">
                    <div class="headeing-content"><h5><i class="fal fa-list-ul me-1"></i> Attribute List</h5></div>
                    <div class="col-12">    
                    <table id="userCategories" class="table table-responsive-sm table-hover table-bordered text-center">
                        <thead class="table-dark">
                            <tr>
                                <th>Id</th>
                                <th>Attribute Name</th>
                                <th>Sort Order</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="table-secondary text-secondary">
                            @foreach($attribute as $attributes)
                                <tr>
                                    <td>{{$attributes->id}}</td>
                                    <td>{{$attributes->attribute_name}}</td>
                                    <td>{{$attributes->sort_order}}</td>
                                    <td class="d-flex justify-content-center">
                                        <?php 
                                            foreach (session()->get('permissionKey') as $key) {
                                                if($key['permission_key'] == 'attribute-update'){ 
                                        ?>
                                        <a href="{{ route('attributes.edit', $attributes->id) }}" class="my-auto me-2"><button class="btn btn-success"><i class="fas fa-edit"></i> Edit </button></a>
                                        <?php 
                                                }
                                                if($key['permission_key'] == 'attribute-delete'){ 
                                        ?>
                                        <form action="{{ route('attributes.destroy', $attributes->id) }}" method="post" class="my-auto">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"><i class="fas fa-trash"></i> Delete</button>
                                        </form>  
                                        <?php } } ?>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>

@endsection