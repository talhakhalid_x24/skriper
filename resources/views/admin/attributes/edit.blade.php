@extends('layouts/admin_web')
<title>Skriper | Attributes | Update</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Update Attributes</h1>
                </div>
                <form action="{{ route('attributes.update' , $attribute['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label">Attibute Name</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="attribute_name" value="{{$attribute['attribute_name']}}">
                    </div>
                    @error('attribute_name')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                    <div class="col-12 my-3">
                        <button class="btn btn-success" type="submit"><i class="fas fa-edit"> </i> Update</button>
                    </div>
                </form>
@endsection