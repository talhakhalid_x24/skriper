@extends('layouts/admin_web')
<title>Skriper | Attributes | Add</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Add Attributes</h1>
                </div>
                <div class="card">
                <div class="headeing-content"><h5 class="m-auto"><i class="fal fa-list-ul me-1"></i> Add attribute</h5></div>
                <form action="{{ route('attributes.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label"><span class="text-danger">*</span> Attibute Name</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="attribute_name">
                    </div>
                    @error('attribute_name')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                    <div class="col-sm-12 my-3">
                        <button class="btn btn-success" type="submit"><i class="fas fa-plus"></i> Add</button>
                    </div>
                </form> 
                </div>
@endsection