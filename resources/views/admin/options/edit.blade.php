@extends('layouts/admin_web')
<title>Skriper | options | Update</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Update Option</h1>
                </div>
                <form action="{{ route('options.update' , $option->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label">Option Name</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="option_name" value="{{$option->option_name}}">
                    </div>
                    @error('option_name')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label">Option Type</label>
                    <div class="input-group has-validation">
                    <input type="text" class="form-control" name="option_type" value="{{$option->option_type}}">
                    </div>
                    @error('option_type')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-sm-12 position-relative">
                    <label for="validationTooltip03" class="form-label">Option Values</label>
                    <a class="btn btn-success text-light float-right" onclick="addOptionValue()">+ Add Another Option Value</a>
                    <div class="input-group has-validation">
                    @foreach($optionValue as $optionValues)
                    <div class="alert alert-dismissible fade show p-0" style="width:100%;">
                        <input type="text" class="form-control" name="option_value[]" value="{{$optionValues->option_value}}">
                        <button type="button" class="close pt-2" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endforeach
                    </div>
                    <div id="add-option-value"></div>
                    @error('option_value')
                        <span class="bg-danger p-2 text-light rounded">{{$message}}</span>
                    @enderror
                </div>
                    <div class="col-12 my-3">
                        <button class="btn btn-success" type="submit"><i class="fas fa-edit"> </i> Update</button>
                    </div>
                </form>
@endsection