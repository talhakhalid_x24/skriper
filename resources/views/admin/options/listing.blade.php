@extends('layouts/admin_web')
<title>Skriper | Options</title>

@section('content')
                <div class="col-12 mb-3 d-flex justify-content-between">
                    <h1>Options</h1>
                        <?php 
                            foreach (session()->get('permissionKey') as $key) {
                                if($key['permission_key'] == 'option-add'){ 
                        ?>
                    <a href="{{ route('options.create') }}"><button class="btn btn-info my-3 text-light"><i class="fas fa-plus"></i> Add</button></a>
                        <?php } }  ?>
                </div>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{Session::get('success')}}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                <div class="card">
                    <div class="headeing-content"><h5><i class="fal fa-list-ul me-1"></i> Option List</h5></div>
                    <div class="col-12">    
                    <table id="userCategories" class="table table-responsive-sm table-hover table-bordered text-center">
                        <thead class="table-dark">
                            <tr>
                                <th>Id</th>
                                <th>Option Name</th>
                                <th>Option Type</th>
                                <th>Sort Order</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="table-secondary text-secondary">
                            @foreach($option as $options)
                                <tr>
                                    <td>{{$options->id}}</td>
                                    <td>{{$options->option_name}}</td>
                                    <td>{{$options->option_type}}</td>
                                    <td>{{$options->sort_order}}</td>
                                    <td class="d-flex justify-content-center">
                                        <?php 
                                            foreach (session()->get('permissionKey') as $key) {
                                                if($key['permission_key'] == 'option-update'){ 
                                        ?>
                                        <a href="{{ route('options.edit', $options->id) }}" class="my-auto me-2"><button class="btn btn-success"><i class="fas fa-edit"></i> Edit </button></a>
                                        <?php 
                                                }
                                                if($key['permission_key'] == 'option-delete'){ 
                                        ?>
                                        <form action="" method="post" class="my-auto">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"><i class="fas fa-trash"></i> Delete</button>
                                        </form>  
                                        <?php } } ?>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>

@endsection