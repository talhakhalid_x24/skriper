@extends('layouts/admin_web')
<title>Skriper | Attribute Values</title>

@section('content')
                <div class="col-12 mb-3">
                    <h1>Attribute Values</h1>
                </div>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{Session::get('success')}}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif
                <div class="card">
                    <div class="headeing-content"><h5><i class="fal fa-list-ul me-1"></i> Attribute Value List</h5></div>
                    <div class="col-12">    
                    <table id="userCategories" class="table table-responsive-sm table-hover table-bordered text-center">
                        <thead class="table-dark">
                            <tr>
                                <th>Id</th>
                                <th>Attribute Name</th>
                                <th>Attribute Value</th>
                            </tr>
                        </thead>
                        <tbody class="table-secondary text-secondary">
                            @foreach($productAttribute as $productAttributes)
                                <tr>
                                    <td>{{$productAttributes['id']}}</td>
                                    <td>{{($productAttributes['attribute']['attribute_name'])}}</td>
                                    <td>{{$productAttributes['attribute_value']}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>

@endsection