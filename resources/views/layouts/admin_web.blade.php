<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Skriper | Dashboard</title>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Main styles for this application-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/@coreui/chartjs/css/coreui-chartjs.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('dist/css/select2.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('https://pro.fontawesome.com/releases/v5.10.0/css/all.css') }}" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <script src="https://cdn.ckeditor.com/ckeditor5/27.0.0/classic/ckeditor.js"></script>
  </head>
  <body class="c-app">
    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
      <div class="c-sidebar-brand" style="font-size:30px;">
        Skriper
      </div>
      <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/admin')}}">
        <i class="far fa-tachometer-alt me-3"> </i> Dashboard</a></li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/admin/users')}}">
        <i class="fal fa-user me-3"></i> Users</a></li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/admin/products')}}">
        <i class="fal fa-images me-3"></i> Products</a></li>
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
          <i class="fal fa-check-circle me-3"></i> Options</a>
            <ul class="c-sidebar-nav-dropdown-items">
              <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/admin/options')}}">
               Options</a></li>
            </ul>
            <ul class="c-sidebar-nav-dropdown-items">
              <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/admin/option-values')}}">
               Option Values</a></li>
            </ul>
        </li>
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
        <i class="fab fa-creative-commons-by me-3"></i> Attributes</a>
            <ul class="c-sidebar-nav-dropdown-items">
              <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/admin/attributes')}}">
              Attributes</a></li>
            </ul>
            <ul class="c-sidebar-nav-dropdown-items">
              <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/admin/attribute-values')}}">
              Attribute Values</a></li>
            </ul>
        </li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/admin/roles')}}">
        <i class="far fa-dice-d6 me-3"></i> Roles</a></li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('/admin/permissions')}}">
        <i class="far fa-universal-access me-3"></i> Permissions</a></li>
      </ul>
      <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
    </div>
    <div class="c-wrapper c-fixed-components">
      <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
          <svg class="c-icon c-icon-lg">
            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-menu')}}"></use>
          </svg>
        </button>
        <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
          <svg class="c-icon c-icon-lg">
            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-menu')}}"></use>
          </svg>
        </button>
        <ul class="c-header-nav ml-auto mr-4">
          <li class="c-header-nav-item dropdown"><a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              <div class="c-avatar">
                @if(session()->get('LoggedIn')['image'])
                <img class="c-avatar-img" src="{{asset('storage/user/'.session()->get('LoggedIn')['image'])}}" alt="user@email.com">
                @else
                <i class="fad fa-user" style="font-size:40px;"></i>
                @endif
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0 mt-2">
              <div class="dropdown-header bg-light py-2"><strong>Account</strong></div>
              <!-- <a class="dropdown-item" href="#">
                <svg class="c-icon mr-2">
                  <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-user')}}"></use>
                </svg> Profile</a> -->
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{url('/admin/profile')}}">
                <svg class="c-icon mr-2">
                  <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-user')}}"></use>
                </svg> Profile
              <a class="dropdown-item" href="{{url('logout')}}">
                <svg class="c-icon mr-2">
                  <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-account-logout')}}"></use>
                </svg> Logout
              </a>
              </a>
            </div>
          </li>
        </ul>
      </header>
      <div class="c-body">
        <main class="c-main">
          <div class="container-fluid">
            <div class="fade-in">
              <div class="row">

      @yield('content')


              </div>
            </div>
          </div>
        </main>
      </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="{{asset('vendors/@coreui/coreui/js/coreui.bundle.min.js')}}"></script>
    <!--[if IE]><!-->
    <script src="{{asset('vendors/@coreui/icons/js/svgxuse.min.js')}}"></script>
    <!--<![endif]-->
    <!-- Plugins and scripts required by this view-->
    <script src="{{asset('vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js')}}"></script>
    <script src="{{asset('vendors/@coreui/utils/js/coreui-utils.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('dist/js/select2.min.js') }}"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
    <script>
    $(document).ready( function () {
        $('#userCategories').DataTable();
    } );

    function createImage() {
      var txtNewInputBox = document.createElement('div');
      txtNewInputBox.setAttribute("class", "input-group has-validation p-0 m-0 alert alert-dismissible fade show");

      txtNewInputBox.innerHTML= '<input type="file" class="form-control" name="product_image[]">'+
          '<button type="button" class="close pt-2" data-dismiss="alert" aria-label="Close">'+
            '<span aria-hidden="true">&times;</span>'+
          '</button>';
      document.getElementById("newElementId1").appendChild(txtNewInputBox);
    }

    function addOptionValue() {
      var txtNewInputBox = document.createElement('div');
      txtNewInputBox.setAttribute("class", "input-group has-validation p-0 m-0 alert alert-dismissible fade show");

      txtNewInputBox.innerHTML= '<input type="text" class="form-control" name="option_value[]">'+
          '<button type="button" class="close pt-2" data-dismiss="alert" aria-label="Close">'+
            '<span aria-hidden="true">&times;</span>'+
          '</button>';
      document.getElementById("add-option-value").appendChild(txtNewInputBox);
    }


    $(".checked").on('click', function (e) {
      const txtNewInputBox = $('.clone').clone().eq(0);
          $(".add-attribute-value").append(txtNewInputBox);
    });

    $(".checked-option").on('click', function (e) {
      const txtNewInputBox = $('.clone-option').clone().eq(0);
        $(".add-option-value").append(txtNewInputBox);
    });

    </script>

    <script>
    function myFunction(imgs) {
      var expandImg = document.getElementById("expandedImg");
      var imgText = document.getElementById("imgtext");
      expandImg.src = imgs.src;
      // imgText.innerHTML = imgs.alt;
      expandImg.parentElement.style.display = "block";
    }
    </script>
    <script>
      $(document).ready(function(){
        $('#selectOption').on('change',function(){
      // console.log($(this).parent().parent().siblings().children('div').children('#selectoptionvalue'));
            var optionId= this.value;
            if (optionId) {
              $.ajax({
                url: "{{url('/admin/products/get-option-values')}}",
                type: "GET",
                dataType: "json",
                data:{
                  option_id :optionId,
                },
                success: function(data){
                // console.log($('#selectOption').parents('.option').siblings('.option-value').children('.input-group').children('select'));
                $('#selectOptionValue').html('<option value="">Select Option Value</option>');
                  $.each(data.optionValue,function(key,value){
                    $('#selectOptionValue').append('<option value="'+value.id+'">'+value.option_value+'</option>');
                  });
                }
              });
            }
        });
      });
    </script>
  </body>
</html>
