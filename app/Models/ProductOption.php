<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    use HasFactory;
    protected $table = 'product_options';
    
    public function option(){
        return $this->belongsTo('App\Models\Option','option_id','id');
    }
}
