<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table='products';

    public function productImages(){
        return $this->hasMany('App\Models\ProductImage','product_id','id');
    } 

    public function productOption(){
        return $this->hasMany('App\Models\ProductOption','product_id','id');
    }

}
