<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    use HasFactory;
    protected $table = 'role_permissions';

    public function all_permissions(){
        return $this->belongsTo('App\Models\Permission','permission_id','id');
    }

    public function permissions(){
        return $this->belongsTo('App\Models\Permission','permission_id','id');
    }
}
