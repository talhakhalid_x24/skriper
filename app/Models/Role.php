<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Role extends Model
{
    protected $table = 'roles';
    use HasFactory;
    
    public function rolePermissions(){
        return $this->hasMany('App\Models\RolePermission','role_id','id');
    }
}
