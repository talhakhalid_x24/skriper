<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductAttribute;

class ProductAttributeController extends Controller
{
    public function index(){
        $productAttribute = ProductAttribute::with('attribute')->get()->toArray();
        // echo "<pre>";
        // print_r($productAttribute);
        // exit();
        
        return view('/admin/attributeValues/listing',compact('productAttribute'));
    }
}
