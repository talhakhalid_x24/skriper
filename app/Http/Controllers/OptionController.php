<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Option;
use App\Models\OptionValue;

class OptionController extends Controller
{
    public function index(){
        $option = Option::all();

        return view('/admin/options/listing',compact('option'));
    }

    public function create(){
        return view('/admin/options/add');
    }

    public function store(Request $req){
        $req->validate([
            'option_name'=>'required',
            'option_type'=>'required',
            'option_value'=>'required',
        ]);

        $option = new Option;
        $option->option_name = $req->option_name;
        $option->option_type = $req->option_type;
        $option->sort_order = 0;
        $option->save();

        $option_id = $option->id;

        foreach ($req->option_value as $option_value) {
            $optionValue = [];
            $optionValue['option_id'] = $option_id;
            $optionValue['option_value'] = $option_value;

            OptionValue::insert($optionValue);
        }

        return redirect('/admin/options');
    }

    public function edit($id){
        $option = Option::find($id);
        $optionValue = OptionValue::where('option_id','=',$id)->get();

        return view('/admin/options/edit',compact('option','optionValue'));
    }

    public function update(Request $req,$id){
        
        $req->validate([
            'option_name'=>'required',
            'option_type'=>'required',
            'option_value'=>'required',
        ]);


        $option = Option::find($id);
        $option->option_name = $req->option_name;
        $option->option_type = $req->option_type;
        $option->sort_order = 0;
        $option->save();

        $option_value = OptionValue::where('option_id','=',$id);
        $option_value->delete();

        $option_id = $option->id;


        foreach ($req->option_value as $option_value) {
            $optionValue = [];
            $optionValue['option_id'] = $option_id;
            $optionValue['option_value'] = $option_value;

            OptionValue::where('option_id','=',$id)->insert($optionValue);
        }

        return redirect('/admin/options');

    }
}
