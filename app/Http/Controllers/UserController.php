<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class UserController extends Controller
{
    public function login(){
        return view('admin/login');
    }

    public function check(Request $req){
        $req->validate([
            'email' => 'required|email',
            'password' => 'required|min:5|max:20'
        ]);

        $user = User::with(['all_role','all_role.rolePermissions','all_role.rolePermissions.permissions'])->where('email','=',$req->email)->where('isAdmin','=','1')->first();
        if($user){
            if(Hash::check($req->password,$user->password)){
                session()->put('LoggedIn',$user);
                        $role = $user->toArray()['all_role']['role_permissions'];
                        // echo "<pre>";
                        foreach ($role as $key) {
                            // print_r();  
                            $permission_key[] = $key['permissions'];
                            session()->put('permissionKey',$permission_key);

                        }
                    // exit();
                return redirect('/admin');
            }
            else{
                return back()->with('fails','Your password is incorrect!');
            }
        }
        else{
            return back()->with('fails','You are not allow to sign in!');
        }
    }

    public function admin(){
        return view('admin/welcome');
    }

    public function logout(){
        if(session()->has('LoggedIn')){
            session()->pull('LoggedIn');
            return redirect('/admin/login');
        }
    }

}
