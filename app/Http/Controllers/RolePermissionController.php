<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RolePermission;

class RolePermissionController extends Controller
{
    public function index(){
        $role_permission = RolePermission::with(['all_roles','all_permissions'])->get()->toArray();
        // echo '<pre>';
        // print_r($role_permission);
        // exit();
        return view('admin/role_permissions/listing',compact('role_permission'));
    }
}
