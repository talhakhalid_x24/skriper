<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission;

class PermissionController extends Controller
{
    public function index(){
        $permission = Permission::all();
        // echo "<pre>";
        // print_r($permission);
        // exit();
        return view('admin/permissions/listing',compact('permission'));
    }

    public function create(){
        return view('admin/permissions/add');
    }

    public function store(Request $req){
        $req->validate([
            'permission_name'=>'required',
            'permission_module'=>'required',
            'permission_key'=>'required',
        ]);
        
        $permission = new Permission;
        $permission->permission_name = $req->permission_name;
        $permission->permission_module = $req->permission_module;
        $permission->permission_key = $req->permission_key;
        $permission->save();
        return redirect('/admin/permissions')->with('success','Permission is successfully added!');
    }

    public function show($id){

    }

    public function edit($id){
        $permission = Permission::find($id);
        return view('admin/permissions/edit',compact('permission'));
    }

    public function update(Request $req,$id){
        $req->validate([
            'permission_name'=>'required',
            'permission_module'=>'required',
            'permission_key'=>'required',
        ]);

        $permission = Permission::find($id);
        $permission->permission_name = $req->permission_name;
        $permission->permission_module = $req->permission_module;
        $permission->permission_key = $req->permission_key;
        $permission->save();
        return redirect('/admin/permissions')->with('success','Permission is successfully updated!');
    }

    public function destroy($id){
        $permission = Permission::find($id);
        $permission->delete();
        return redirect('/admin/permissions')->with('success','Permission is successfully deleted!');
    }
}
