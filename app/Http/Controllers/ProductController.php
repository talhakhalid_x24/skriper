<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\ProductOption;
use App\Models\ProductOptionValue;
use App\Models\Attribute;
use App\Models\ProductAttribute;
use DB; 

class ProductController extends Controller
{
    public function index(){
        $product = Product::with(['productImages'])
        ->paginate(6);
        // ->get()->toArray();
        // echo '<pre>';
        // print_r($product);
        // exit();
        return view('/admin/products/listing',compact('product'));
    }

    public function show($id){
        $product = Product::with('productImages')->where('id','=',$id)->get();

        // echo '<pre>';
        // print_r($product);
        // exit();
        return view('/admin/products/view',compact('product'));
    }

    public function create(Request $req){
        // $productOptionValue = Product::with(['productOption','productOption.option','productOption.option.optionValue'])
        // ->get()
        // ->toArray();
        $option = Option::all();
        $attribute = Attribute::all();

        // echo '<pre>';
        // print_r($productOptionValue);
        // exit();
        return view('/admin/products/add',compact('option','attribute'));
    }

    public function store(Request $req){

        $req->validate([
            'product_name'=>'required',
            'product_description'=>'required',
            'price'=>'required',
            'link'=>'required',
            'product_image'=>'required',
            'option'=>'required',
            'option_value'=>'required',
            'attribute'=>'required',
            'attribute_value'=>'required',
        ]);
        $product = new Product;
        $product->product_name = $req->product_name;
        $product->product_description = $req->product_description;
        $product->price = $req->price;
        $product->link = $req->link;
        $product->save();

        $product_id = $product['id'];
        $price = $product['price'];
        if($req->hasfile('product_image')){
            foreach ($req->file('product_image') as $images) {
                $tempData = [];
                $tempData['product_id'] = $product_id;

                $ext = $images->getClientOriginalName();
                $name = time() . '.' . $ext;
                $images->storeAs('public/product_images/',$name);
                $tempData['product_images'] = $name;

                $tempData['is_featured'] = 0;

                ProductImage::insert($tempData);
            }
        }

        foreach ($req['option'] as $key => $option) {
            $productOption = new ProductOption;
            $productOption->product_id = $product_id;
            $productOption->option_id = $option;
            $productOption->save();

            $productOptionValue = new ProductOptionValue;
            $productOptionValue->product_id = $product_id;
            $productOptionValue->option_id = $option;
            $productOptionValue->product_option_id = $productOption->id;
            $productOptionValue->option_value_id = $req['option_value'][$key];
            $productOptionValue->price = $price;
            $productOptionValue->save();
        }

        foreach ($req->attribute as $key => $attribute) {
            $attributeValue = [];
            $attributeValue['product_id'] = $product_id;
            $attributeValue['attribute_id'] = $attribute;
            $attributeValue['attribute_value'] = $req->attribute_value[$key];

            ProductAttribute::insert($attributeValue);
        }

        return redirect('/admin/products')->with('success','Product is successfully added!');
    }

    public function edit($id){
        $products = Product::find($id);
        $product_images = ProductImage::where('product_id','=',$id)->get()->toArray();
        $option = Option::all();
        $productOption = ProductOption::where('product_id','=',$id)->get('option_id');
        $attribute = Attribute::all();
        $productAttribute = ProductAttribute::where('product_id','=',$id)->get();

        // echo '<pre>';
        // print_r($productOption);
        // exit();
        return view('/admin/products/edit',compact('products','product_images','option','productOption','attribute','productAttribute'));
    }

    public function update(Request $req,$id){
        $req->validate([
            'product_name'=>'required',
            'product_description'=>'required',
            'price'=>'required',
            'link'=>'required',
            'product_image'=>'required',
            'option'=>'required',
            'attribute'=>'required',
            'attribute_value'=>'required',
        ]);

        $product = Product::find($id);
        $product->product_name = $req->product_name;
        $product->product_description = $req->product_description;
        $product->price = $req->price;
        $product->link = $req->link;
        $product->save();

        $product_id = $product['id'];
        
        $productImage = ProductImage::where('product_id','=',$id);
        $productImage->delete();

        if($req->hasfile('product_image')){
            foreach ($req->file('product_image') as $images) {
                $tempData = [];
                $tempData['product_id'] = $product_id;

                $ext = $images->getClientOriginalName();
                $name = time() . '.' . $ext;
                $images->storeAs('public/product_images/',$name);
                $tempData['product_images'] = $name;

                $tempData['is_featured'] = 0;

                ProductImage::where('product_id','=',$id)->insert($tempData);
            }
        }
        
        ProductOption::where('product_id','=',$id)->delete();

        foreach ($req->option as $option) {
            $optionValue = [];
            $optionValue['product_id'] = $product_id;
            $optionValue['option_id'] = $option;
            
            ProductOption::where('product_id','=',$id)->insert($optionValue);
        }

        ProductAttribute::where('product_id','=',$id)->delete();

        foreach ($req->attribute as $key => $attribute) {
            $attributeValue = [];
            $attributeValue['product_id'] = $product_id;
            $attributeValue['attribute_id'] = $attribute;
            $attributeValue['attribute_value'] = $req->attribute_value[$key];

            ProductAttribute::insert($attributeValue);
        }

        return redirect('/admin/products')->with('success','Product is successfully updated!');
    }

    public function destroy($id){
        $product = Product::find($id);
        $product->delete();

        $productImage = ProductImage::where('product_id','=',$id);
        $productImage->delete();
        
        $optionValue = ProductOption::where('product_id','=',$id);
        $optionValue->delete();

        $productAttribute = ProductAttribute::where('product_id','=',$id);
        $productAttribute->delete();

        return redirect('/admin/products')->with('success','Product is successfully deleted!');
    }

    public function getOptionValues(Request $request){
        $option_id = $request->option_id;
        $optionValue= OptionValue::where("option_id",$option_id)->get();

        return response()->json(['optionValue'=>$optionValue]);
    }

}