<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attribute;

class AttributeController extends Controller
{
    public function index(){
        $attribute = Attribute::all();

        return view('/admin/attributes/listing',compact('attribute'));
    }

    public function create(){
        return view('/admin/attributes/add');
    }

    public function store(Request $req){
        $req->validate([
            'attribute_name'=>'required',
        ]);

        $attribute = new Attribute;
        $attribute->attribute_name = $req->attribute_name;
        $attribute->sort_order = 0;
        $attribute->save();

        return redirect('/admin/attributes')->with('success','Attribute is successfully added!');
    }

    public function edit($id){
        $attribute = Attribute::find($id);
        // echo '<pre>';
        // print_r($attribute);
        // exit();

        return view('/admin/attributes/edit',compact('attribute'));
    }

    public function update(Request $req,$id){
        $req->validate([
            'attribute_name'=>'required',
        ]);

        $attribute = Attribute::find($id);
        $attribute->attribute_name = $req->attribute_name;
        $attribute->sort_order = 0;
        $attribute->save();

        return redirect('/admin/attributes')->with('success','Attribute is successfully updated!');
    }

    public function destroy($id){
        $attribute = Attribute::find($id);
        $attribute->delete();

        return redirect('/admin/attributes')->with('success','Attribute is successfully deleted!');
    }
}
