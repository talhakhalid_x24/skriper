<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use App\Models\RolePermission;

class RoleController extends Controller
{
    public function index(){
        $role = Role::all();
        $role_permission = RolePermission::with('all_permissions')
        ->get()
        ->toArray();
        // echo "<pre>";
        // print_r($role_permission);
        // exit();
        return view('admin/roles/listing',compact('role','role_permission'));
    }

    public function create(){
        $permission = Permission::all();
        return view('admin/roles/add',compact('permission'));
    }

    public function store(Request $req){
        $req->validate([
            'role'=>'required',
        ]);

        $role = new Role;
        $role->role = $req->role;

        $role->save();

        $role_id = $role->id;
        foreach($req->permissions as $key => $permissions){
            $permission = [];
            $permission['role_id'] = $role_id;
            $permission['permission_id'] = $permissions;
     
            RolePermission::insert($permission);
        }
        return redirect('/admin/roles')->with('success','Role is successfully added!');
    }

    public function show($id){

    }

    public function edit($id){
        $roles = Role::find($id);
        $permission = Permission::all();
        $role_permission = RolePermission::where('role_id','=',$id)->get();
        return view('admin/roles/edit',compact('roles','permission','role_permission'));
    }

    public function update(Request $req,$id){
        $req->validate([
            'role'=>'required',
        ]);

        $roles = Role::find($id);
        $roles->role = $req->role;

        $roles->save();
        $role_permission = RolePermission::where('role_id','=',$id);
        $role_permission->delete();
        $roles_id = $roles->id;
        foreach($req->permissions as $key => $permissions){
            $permission = [];
            $permission['role_id'] = $roles_id;
            $permission['permission_id'] = $permissions;
     
            RolePermission::where('role_id','=',$id)->insert($permission);
        }
        return redirect('/admin/roles')->with('success','Role is successfully updated!');
    }

    public function destroy($id){
        $roles = Role::find($id);
        $roles->delete();
        $role_permission = RolePermission::where('role_id','=',$id);
        $role_permission->delete();
        return redirect('/admin/roles')->with('success','Role is successfully deleted!');

    }

}
