<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class ProfileController extends Controller
{
    public function index(){
        return view('admin/profile/listing');
    }

    public function update(Request $req,$id){

        $user = User::find($id);
        if($req->hasfile('image')){
            $image = $req->file('image');
            $ext = $image->getClientOriginalName();
            $name = time() . '.' . $ext;
            $image->storeAs('public/user/',$name);
            $user->image = $name;
        }
        else if($req->has('name') || $req->has('email')){
            $user->name = $req->name;
            $user->email = $req->email;
        }
        else if($req->has('old_pass')){
            if(Hash::check($req->old_pass,$user->password)){
                if($req->new_pass == $req->con_pass){
                    $user->password = Hash::make($req->new_pass);
                }
                else{
                    return redirect('/admin/profile')->with('err','Your new password and confirm password does not match!');
                }
            }
            else{
                return redirect('/admin/profile')->with('err','Your old password is wrong!');
            }
        }

        $user->save();
        return redirect('/admin/profile')->with('success','Your profile is successfully updated! Logout your profile and then login for changes!');
    }

    public function profile(Request $req, $id){
        
    }
    

}
