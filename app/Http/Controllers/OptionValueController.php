<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OptionValue;

class OptionValueController extends Controller
{
    public function index(){
        $option_value = OptionValue::with(['option'])->get()->toArray();
        // echo '<pre>';
        // print_r($option_value);
        // exit();
        return view('/admin/optionValues/listing',compact('option_value'));
    }
}
