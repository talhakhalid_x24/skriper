<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\RolePermission;
use Hash;

class AllUserController extends Controller
{
    public function index(){
        $user = User::all();
        // echo "<pre>";
        // print_r($role);
        // exit();
        return view('admin/users/listing',compact('user'));
    }

    public function create(){
        $roles = Role::all()->sortByDesc('id');
        return view('admin/users/add',compact('roles'));
    }

    public function store(Request $req){
        $req->validate([
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required|min:5|max:15',
            'role_id'=>'required',
        ]);

        $user = new User;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        if($req->hasfile('image')){
            $image = $req->file('image');
            $ext = $image->getClientOriginalName();
            $name = time() . '.' . $ext;
            $image->storeAs('public/user/',$name);
            $user->image = $name;
        }
        // else{
        //     echo "<pre>";
        //     echo "what the hell";
        //     exit();
        // }
        $user->role_id = $req->role_id;
        $user->isAdmin = 0;

        $user->save();
        return redirect('/admin/users')->with('success','User is successfully added!');
    }

    public function show($id){

    }

    public function edit($id){
        $users = User::find($id);
        $roles = Role::all()->sortByDesc('id');
        return view('admin/users/edit',compact('roles','users'));
    }

    public function update(Request $req,$id){
        $req->validate([
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required|min:5|max:15',
            'role_id'=>'required',
            'isAdmin'=>'required',
        ]);

        $user = User::find($id);
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        if($req->hasfile('image')){
            $image = $req->file('image');
            $ext = $image->getClientOriginalName();
            $name = time() . '.' . $ext;
            $image->storeAs('public/user/',$name);
            $user->image = $name;
        }
        $user->role_id = $req->role_id;
        $user->isAdmin = $req->isAdmin;

        $user->save();
        return redirect('/admin/users')->with('success','Users is successfully updated!');
    }

    public function destroy($id){
        $users = User::find($id);
        $users->delete();
        return redirect('/admin/users')->with('success','User is successfully deleted!');

    }
}
